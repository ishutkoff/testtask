import './import/blocks.js';

var weather = new Vue({
  el: '#weather',
  data: {
    enterLocation: false,
    loading: true,
    autolocation: false,
    ttype: 'C',
    location:
      {
        strCityName: ''
      }
    ,
    locations: [],
    weather: [],
  },
  mounted: function () {
    this.curLocation();
  },
  methods: {
    curLocation () {
      ymaps.ready(function () {
        ymaps.geolocation.get({
          provider: 'yandex',
          autoReverseGeocode: true
        }).then(function (result) {
          weather.location.strCityName = result.geoObjects.get(0).properties.get('metaDataProperty.GeocoderMetaData.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName');
          weather.autolocation = true;
        });
      });
    },
    changeType (type) {
      this.ttype = type;
    },
    changeLocation (location) {
      this.location = location;
      this.enterLocation = false;
      this.weather = [];
      this.getWeather(weather.location.strSuggestionId);
    },
    focusLocation (focus) {
      if (focus) {
        this.enterLocation = true;
      } else {
        setTimeout(function () {
          this.enterLocation = false;
        }, 200);
      }
    },
    getLocation: function (location) {
      if (location) {
        clearTimeout(sTimeOut);
        var sTimeOut = setTimeout(function () {
          $.ajax({
            method: 'GET',
            dataType: 'json',
            url: 'http://176.53.163.46/api/city-suggestions?text=' + location,
            success: function (data) {
              if (weather.autolocation) {
                weather.location = data[0];
                weather.getWeather(weather.location.strSuggestionId);
                weather.autolocation = false;
                weather.loading = false;
              }
              weather.locations = data;
            },
          });
        }, 300);
      }
    },
    getWeather (location) {
      if (location) {
        clearTimeout(sTimeOut);
        var sTimeOut = setTimeout(function () {
          $.ajax({
            method: 'GET',
            dataType: 'json',
            url: 'http://176.53.163.46/api/weather?suggestion_id=' + location + '&units=metric&locale=ru',
            success: function (data) {
              weather.weather = data;
            },
          });
        }, 2000);
      }
    },
    getCityWeather () {
      this.getWeather(this.location.strSuggestionId);
      this.enterLocation = false;
    }
  },
  computed: {
    getTemp: function () {
      if (this.ttype === 'C') {
        return Math.round(this.weather.fTemperature);
      } else if (this.ttype === 'F') {
        return Math.round((this.weather.fTemperature * 9 / 5) + 32);
      } else {
        return 0;
      }
    }
  },
  watch: {
    'location.strCityName': function (newLocation, oldLocation) {
      this.getLocation(this.location.strCityName);
      if (this.location.strCityName === '') {
        this.locations = [];
        this.location = [];
        this.weather = [];
      }
    }
  }
});